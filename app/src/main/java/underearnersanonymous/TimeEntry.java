package underearnersanonymous;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import underearnersanonymous.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class TimeEntry extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private static final String TAG = DialogFragment.class.getSimpleName();
    public static final String POSITION = "position";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORY = "category";
    private EditText etDescription;
    private RadioGroup rgCategories;
    private Spinner spTimeSlotTo, spTimeSlotFrom;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.time_entry, container, false);
        getDialog().setTitle(R.string.time_entry);
        etDescription = (EditText) rootView.findViewById(R.id.description);
        spTimeSlotTo = (Spinner) rootView.findViewById(R.id.timeslot_to);
        spTimeSlotFrom = (Spinner) rootView.findViewById(R.id.timeslot_from);
        rgCategories = (RadioGroup) rootView.findViewById(R.id.categories);

        int pos = -1;
        String description = "", category = "";
        if(getArguments() != null) {
            pos = getArguments().getInt(TimeEntry.POSITION);
            description = getArguments().getString(DESCRIPTION, "");
            category = getArguments().getString(CATEGORY, "");
        }

        etDescription.setText("");
        etDescription.append(description);
        for (int i=0; i < rgCategories.getChildCount(); i++){
            if(((RadioButton) rgCategories.getChildAt(i)).getText().equals(category)){
                ((RadioButton) rgCategories.getChildAt(i)).setChecked(true);
            }
        }

        Button confirm = (Button) rootView.findViewById(R.id.confirm);

        spTimeSlotFrom.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, getFromTimeSlots()));
        spTimeSlotTo.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, getToTimeSlots()));
        spTimeSlotFrom.setOnItemSelectedListener(this);
        if(pos != -1){
            spTimeSlotFrom.setSelection(pos);
        } else {
            spTimeSlotFrom.setSelection(getCurrentTimeIdx());
        }
        confirm.setOnClickListener(this);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return rootView;
    }

    private ArrayList<String> getFromTimeSlots(){
        ArrayList<String> timeSlots = new ArrayList<>();
        for(int i = 5; i <= 11; i++){
            timeSlots.add(i+":00 am");
            timeSlots.add(i+":15 am");
            timeSlots.add(i+":30 am");
            timeSlots.add(i+":45 am");
        }

        timeSlots.add("12:00 pm");
        timeSlots.add("12:15 pm");
        timeSlots.add("12:30 pm");
        timeSlots.add("12:45 pm");

        for(int i = 1; i <= 11; i++){
            timeSlots.add(i+":00 pm");
            timeSlots.add(i+":15 pm");
            timeSlots.add(i+":30 pm");
            timeSlots.add(i+":45 pm");
        }

        timeSlots.add("12:00 am");
        timeSlots.add("12:15 am");
        timeSlots.add("12:30 am");
        timeSlots.add("12:45 am");

        return timeSlots;
    }

    private ArrayList<String> getToTimeSlots(){
        ArrayList<String> timeSlots = new ArrayList<>();
        for(int i = 5; i <= 11; i++){
            timeSlots.add(i + ":15 am");
            timeSlots.add(i + ":30 am");
            timeSlots.add(i + ":45 am");
            timeSlots.add((i + 1) + ":00 am");
        }

        timeSlots.add("12:15 pm");
        timeSlots.add("12:30 pm");
        timeSlots.add("12:45 pm");
        timeSlots.add("1:00 pm");

        for(int i = 1; i <= 10; i++){
            timeSlots.add(i + ":15 pm");
            timeSlots.add(i + ":30 pm");
            timeSlots.add(i + ":45 pm");
            timeSlots.add((i + 1) + ":00 pm");
        }

        timeSlots.add("11:15 pm");
        timeSlots.add("11:30 pm");
        timeSlots.add("11:45 pm");
        timeSlots.add("12:00 am");

        timeSlots.add("12:15 am");
        timeSlots.add("12:30 am");
        timeSlots.add("12:45 am");
        timeSlots.add("1:00 am");

        return timeSlots;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirm:
                if(validEntry()) {
                    TimeSlotInterface activity = (TimeSlotInterface) getActivity();
                    int radioButtonID = rgCategories.getCheckedRadioButtonId();
                    RadioButton radioButton = (RadioButton) rgCategories.findViewById(radioButtonID);
                    activity.applyEntry(etDescription.getText().toString(), getTimeSlotPosition(), radioButton == null ? "" : radioButton.getText().toString());
                    this.dismiss();
                }
                break;
        }
    }

    private ArrayList<Integer> getTimeSlotPosition() {
        ArrayList<Integer> slots = new ArrayList<>();
        ArrayList<String> fromSlots = getFromTimeSlots();
        int idx = spTimeSlotFrom.getSelectedItemPosition();
        slots.add(idx++);
        String currDate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
        Date toTime = null, fromTime = null;
        try {
            toTime = new SimpleDateFormat("dd.MM.yyyy hh:mm a", Locale.getDefault()).parse(currDate + " " + spTimeSlotTo.getSelectedItem().toString());
            fromTime = new SimpleDateFormat("dd.MM.yyyy hh:mm a", Locale.getDefault()).parse(currDate + " " + fromSlots.get(idx));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        while(fromTime.before(toTime) || idx >= fromSlots.size()){
            slots.add(idx++);
            try {
                fromTime = new SimpleDateFormat("dd.MM.yyyy hh:mm a", Locale.getDefault()).parse(currDate + " " + fromSlots.get(idx));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        return slots;
    }


    private boolean validEntry() {
        if(etDescription.getText().toString().equals("")){
            etDescription.setError(getString(R.string.enter_description));
            return false;
        }

        return true;
    }

    public int getCurrentTimeIdx() {
        Date time = new Date();
        ArrayList<String> fromTimeSlots = getFromTimeSlots();
        for(int i=0; i<fromTimeSlots.size(); i++){
            String timeSlot = fromTimeSlots.get(i);
            Date before = getTime(timeSlot);
            if(i+1>=fromTimeSlots.size()){
                return i;
            }
            Date after = getTime(fromTimeSlots.get(i+1));
            if(time.after(before) && time.before(after)){
                return i;
            }
        }
        return 0;
    }

    private Date getTime(String before) {
        Date date = new Date();
        try {
            String calDate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(date);
            date = new SimpleDateFormat("dd.MM.yyyy hh:mm a", Locale.getDefault()).parse(calDate + " " + before);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int idx = spTimeSlotFrom.getSelectedItemPosition();
        ArrayList<String> toTimeSlots = getToTimeSlots();
        spTimeSlotTo.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, toTimeSlots.subList(idx, toTimeSlots.size())));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
