package underearnersanonymous;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeSheetActivity extends AppCompatActivity implements View.OnClickListener, TimeSlotInterface {

    private static final int CELL_PADDING = 2;
    private static final String TAG = TimeSheetActivity.class.getSimpleName();
    private ArrayList<CompositeLinearLayout> mViews;
    private TimeSheetPresenter mPresenter;
    private Date mCurrentDate;
    private boolean isDisplayDescription = true;
    private TableLayout mTable;
    private RelativeLayout mLinearLayout;
    private int mHeight = 33;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String name = preferences.getString(Constants.MODE, Constants.TIMERECORDING);
        if(name.equals(Constants.SOLVENCY)){
            startActivity(new Intent(this, SolvencyActivity.class));
        }

        setContentView(R.layout.activity_time_sheet);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.timesheet);

        mTable = (TableLayout) findViewById(R.id.table_layout);
        mLinearLayout = (RelativeLayout) findViewById(R.id.container_layout);
        mPresenter = new TimeSheetPresenterImpl(this, this);
        ImageView btnTime = (ImageView) findViewById(R.id.btn_time);
        ImageView btnMoney = (ImageView) findViewById(R.id.btn_money);

        btnMoney.setClickable(true);

        btnMoney.setOnClickListener(this);

        mCurrentDate = new Date();
        initHeight();
        initTable();

        ImageView fab = (ImageView) findViewById(R.id.fab);
        fab.setClickable(true);
        fab.setOnClickListener(this);

        initLongClickListener();
        initOnClickListener();

        loadDataFromDB();

        mLinearLayout.setOnTouchListener(new OnSwipeTouchListener(this){
            public void onSwipeTop() {
//                isDisplayDescription = true;
//                loadNewSheet(0);
            }
            public void onSwipeRight() {
                loadNewSheet(-1);
            }
            public void onSwipeLeft() {
                loadNewSheet(+1);
            }
            public void onSwipeBottom() {
//                isDisplayDescription = false;
//                loadNewSheet(0);
            }

        });
    }

    private void initHeight() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        mHeight = height/29;
    }

    private void loadNewSheet(int i) {
        mCurrentDate = newDate(mCurrentDate, i);
        resetTimesheet();
        loadDataFromDB();
    }

    private Date newDate(Date mCurrentDate, int i) {
        Calendar currentCal = DateToCalendar(mCurrentDate);
        currentCal.add(Calendar.DATE, i);
        Date date = new Date(currentCal.getTimeInMillis());
        Log.d(TAG, date.toString());
        return date;
    }

    public static Calendar DateToCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private Spanned getCategory(String code){
        switch (code){
            case "B": return Html.fromHtml("<font color='#5C00FF'>Business</font>");
            case "S": return Html.fromHtml("<font color='#FF0000'>Service</font>");
            case "R": return Html.fromHtml("<font color='#00FFFF'>Recreatio</font>");
            case "V": return Html.fromHtml("<font color='#FC2AFA'>Vision</font>");
            case "SC": return Html.fromHtml("<font color='#00FF20'>SelfCare</font>");
            default: return Html.fromHtml("");
        }
    }

    private void loadDataFromDB() {

    ArrayList<CompositeTextView> entries = mPresenter.loadData(Integer.parseInt(new SimpleDateFormat(Constants.DBDATE, Locale.getDefault()).format(mCurrentDate)));
        for(int i = 0; i<entries.size(); i++) {
            if (entries.get(i) != null) {
                mViews.get(i).setDescription(entries.get(i).getText().toString());
                mViews.get(i).setCategory(entries.get(i).getCategory());
                if(isDisplayDescription) {
                    mViews.get(i).setText(entries.get(i).getText());
                }
                else {
                    mViews.get(i).setText(getCategory(entries.get(i).getCategory()));
                }

            }
        }

    }

    private void initOnClickListener() {
        for(int i = 0; i < mViews.size(); i++){
            final int pos = i;
            mViews.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimeEntry timeEntry = new TimeEntry();
                    Bundle bundle = new Bundle();
                    bundle.putString(TimeEntry.DESCRIPTION, mViews.get(pos).getDescription());
                    bundle.putString(TimeEntry.CATEGORY, mViews.get(pos).getCategory());
                    bundle.putInt(TimeEntry.POSITION, pos);
                    timeEntry.setArguments(bundle);
                    FragmentManager fm = getFragmentManager();
                    timeEntry.show(fm, "Time Entry");
                }
            });
        }
    }

    private void initLongClickListener() {
        for (int i = 1; i < mViews.size(); i++) {
            final int pos = i;
            mViews.get(i).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(mViews.get(pos).getText().equals("")) {
                        String description = mViews.get(pos-1).getText().toString();
                        mViews.get(pos).setCategory(mViews.get(pos-1).getCategory());
                        mViews.get(pos).setDescription(description);
                        if(isDisplayDescription)
                            mViews.get(pos).setText(description);
                        else
                            mViews.get(pos).setText(getCategory(mViews.get(pos-1).getCategory()));
                        mPresenter.insertTimeEntry(getDate(), pos, description, ((CompositeTextView) mViews.get(pos).getChildAt(0)).getCategory());
                    } else {
                        mViews.get(pos).setText("");
                        mViews.get(pos).setDescription("");
                        mViews.get(pos).setCategory("");
                        mPresenter.insertTimeEntry(getDate(), pos, "", "");
                    }
                    return true;
                }
            });
        }
    }

    private void initTable() {
        mViews = new ArrayList<>();
        mTable.removeAllViews();
        initHeader();
        for(int i=5; i<=11; i++) {
            addTableRow(i+"am", i % 2 == 1 ? R.drawable.cell_yellow : R.drawable.cell_white);
        }
        addTableRow("12 noon", R.drawable.cell_white);

        for(int i=1; i<=11; i++) {
            addTableRow(i+"pm", i % 2 == 1 ? R.drawable.cell_yellow : R.drawable.cell_white);
        }
        addTableRow("bed", R.drawable.cell_white);

    }

    private void initHeader() {

        initTimesheetLabel();
        initDateLabel();
    }

    private void initDateLabel() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

        final SpannableStringBuilder date = new SpannableStringBuilder(" Date " + dateFormat.format(mCurrentDate));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        date.setSpan(bss, 0, 5, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        TableRow row = new TableRow(this);
        LinearLayout view = initHeaderView("", 180, Gravity.LEFT);
        ((TextView) view.getChildAt(0)).setText(date);
        ((TextView) view.getChildAt(0)).setTypeface(Typeface.DEFAULT);
        row.addView(view);
        TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
        params.span = 3;
        view.setLayoutParams(params); // causes layout update
        View dayView = initHeaderView(dayFormat.format(mCurrentDate), 120, Gravity.RIGHT);
        row.addView(dayView);
        TableRow.LayoutParams params1 = (TableRow.LayoutParams) dayView.getLayoutParams();
        params1.span = 2;
        dayView.setLayoutParams(params1);
        mTable.addView(row);
    }

    private void initTimesheetLabel() {
        TableRow rowHeader = new TableRow(this);
        ImageView left = new ImageView(this);
        left.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.left));
        left.setClickable(true);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewSheet(-1);
            }
        });
        rowHeader.addView(left);
        TableRow.LayoutParams params = (TableRow.LayoutParams) left.getLayoutParams();
        params.gravity = Gravity.LEFT;
        left.setLayoutParams(params); // causes layout update
        View view = initHeaderView("Timesheet", 110, Gravity.CENTER);
        rowHeader.addView(view);
        params = (TableRow.LayoutParams) view.getLayoutParams();
        params.span = 3;
        view.setLayoutParams(params); // causes layout update
        ImageView right = new ImageView(this);
        right.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.right));
        right.setClickable(true);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewSheet(1);
            }
        });
        rowHeader.addView(right);
        params = (TableRow.LayoutParams) right.getLayoutParams();
        params.gravity = Gravity.RIGHT;
        right.setLayoutParams(params);
        mTable.addView(rowHeader);
    }

    private void addTableRow(String text, int background) {
        TableRow rowHeader = new TableRow(this);
        rowHeader.addView(initTextView(text, 95, R.drawable.cell_white, Gravity.LEFT));
        CompositeLinearLayout tempTv = initTextView("", background);
        mViews.add(tempTv);
        rowHeader.addView(tempTv);
        tempTv = initTextView("", background);
        mViews.add(tempTv);
        rowHeader.addView(tempTv);
        tempTv = initTextView("", background);
        mViews.add(tempTv);
        rowHeader.addView(tempTv);
        tempTv = initTextView("", background);
        mViews.add(tempTv);
        rowHeader.addView(tempTv);
        mTable.addView(rowHeader);
    }

    private CompositeLinearLayout initTextView(String text, int background) {
        return initTextView(text, 95, background, Gravity.CENTER);
    }

    private CompositeLinearLayout initHeaderView(String text, int width, int gravity){
        CompositeLinearLayout layout  = initTextView(text, width, 0, Gravity.CENTER);
        layout.setTypeface(null, Typeface.BOLD);
        layout.setTextColor(ContextCompat.getColor(this, R.color.black));
        layout.setGravity(gravity);
        return layout;
    }

    private CompositeLinearLayout initTextView(String text, int width, int background, int gravity) {
        CompositeLinearLayout layout = new CompositeLinearLayout(this);

        layout.setText(text);
        layout.setWidth(width);
        layout.setSingleLine();
        layout.setPadding(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING);
        if(background != 0) {
            layout.setBackgroundResource(background);
        }
        layout.setMinimumHeight(mHeight);
        layout.setGravity(gravity);
        return layout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_time_sheet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){

        //noinspection SimplifiableIfStatement
//        case R.id.action_save:
//            takeScreenshot();
//            return true;
        case R.id.action_report:
            PieChartFragment fragment = new PieChartFragment();
            ArrayList<Integer> entries = getCategorySummary();
            Bundle bundle = new Bundle();
            bundle.putIntegerArrayList(PieChartFragment.CATEGORIES, entries);
            fragment.setArguments(bundle);
            FragmentManager fm = getFragmentManager();
            fragment.show(fm, "Pie Chart");
        return true;
        case R.id.action_features:
            FeaturesFragment fragment1 = new FeaturesFragment();
            FragmentManager fm1 = getFragmentManager();
            fragment1.show(fm1, "Hidden Features");
        return true;
        default:
            return super.onOptionsItemSelected(item);
        }

    }

    private ArrayList<Integer> getCategorySummary() {
        int tdCnt = 0, bCnt = 0, vCnt = 0, rCnt = 0, sCnt = 0, scCnt = 0;
        for (int i = 0; i < mViews.size(); i++) {
             String category = mViews.get(i).getCategory();

            if(category == null || category.equals("")){
              tdCnt++;
            } else if(category.equals("B")){
                bCnt++;
            } else if(category.equals("V")){
                vCnt++;
            } else if(category.equals("R")){
                rCnt++;
            } else if(category.equals("S")){
                sCnt++;
            } else if (category.equals("SC")){
                scCnt++;
            }
        }
        return new ArrayList<>(Arrays.asList(tdCnt, bCnt, vCnt, rCnt, sCnt, scCnt));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab:
                TimeEntry timeEntry = new TimeEntry();
                FragmentManager fm = getFragmentManager();
                timeEntry.show(fm, "Time Entry");
                break;
            case R.id.btn_money:
                Utils.saveStateToPreferences(this, Constants.SOLVENCY);
                startActivity(new Intent(this, SolvencyActivity.class));
                break;
            case R.id.btn_time:
                break;
        }
        }

    private void resetTimesheet() {
        initTable();
        initLongClickListener();
        initOnClickListener();
    }

    @Override
    public void applyEntry(String description, ArrayList<Integer> slots, String category) {
        for(Integer pos: slots) {
            if (isDisplayDescription) {
                mViews.get(pos).setText(description);
            } else {
                mViews.get(pos).setText(getCategory(category));
            }
            mViews.get(pos).setDescription(description);
            mViews.get(pos).setCategory(category);
            mPresenter.insertTimeEntry(getDate(), pos, description, category);
        }
    }

    private int getDate() {
        return Integer.parseInt(new SimpleDateFormat(Constants.DBDATE, Locale.getDefault()).format(mCurrentDate));
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }
}
