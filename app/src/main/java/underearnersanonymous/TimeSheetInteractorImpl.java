package underearnersanonymous;

import android.content.Context;

import java.util.ArrayList;

public class TimeSheetInteractorImpl implements TimeSheetInteractor {
    private final TimeSheetPresenter mPresenter;
    private final MySQLiteHelper mDB;

    public TimeSheetInteractorImpl(Context context, TimeSheetPresenter timeSheetPresenter) {
        mPresenter =  timeSheetPresenter;
        mDB = MySQLiteHelper.getInstance(context);
    }

    @Override
    public ArrayList<CompositeTextView> loadData(int date) {
        return mDB.loadData(date);
    }

    @Override
    public void insertTimeEntry(int date, int position, String description, String category) {
        mDB.insertTimeEntry(date, position, description);
        mDB.insertCategory(date, position, category);
    }
}
