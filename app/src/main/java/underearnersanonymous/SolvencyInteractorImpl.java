package underearnersanonymous;

import android.content.Context;

import java.util.ArrayList;

public class SolvencyInteractorImpl implements SolvencyInteractor {
    private final SolvencyPresenter mPresenter;
    private final MySQLiteHelper mDBHelper;

    public SolvencyInteractorImpl(Context context, SolvencyPresenter presenter) {
        mPresenter = presenter;
        mDBHelper = MySQLiteHelper.getInstance(context);
    }

    @Override
    public long addExpense(String date, String description, String category, String amount) {
        return mDBHelper.addExpense(date, description, category, amount);
    }

    @Override
    public ArrayList<Expense> loadExpenses() {
        return mDBHelper.loadExpenses();
    }

    @Override
    public void deleteExpense(long id) {
        mDBHelper.deleteExpense(id);
    }

    @Override
    public ArrayList<Expense> getExpensesForMonth() {
        return mDBHelper.getExpensesForMonth();
    }
}
