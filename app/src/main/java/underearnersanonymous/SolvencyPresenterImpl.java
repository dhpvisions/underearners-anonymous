package underearnersanonymous;

import java.util.ArrayList;

public class SolvencyPresenterImpl implements SolvencyPresenter {
    private final SolvencyInteractor mInteractor;
    private final SolvencyActivity mView;

    public SolvencyPresenterImpl(SolvencyActivity activity) {
        mInteractor = new SolvencyInteractorImpl(activity, this);
        mView = activity;
    }

    @Override
    public long addExpense(String date, String description, String category, String amount) {
        return mInteractor.addExpense(date, description, category, amount);
    }

    @Override
    public ArrayList<Expense> loadExpenses() {
        return mInteractor.loadExpenses();
    }

    @Override
    public void deleteExpense(long _id) {
        mInteractor.deleteExpense(_id);
    }

    @Override
    public ArrayList<Expense> getExpensesForMonth() {
        return mInteractor.getExpensesForMonth();
    }
}
