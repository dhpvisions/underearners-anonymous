package underearnersanonymous;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.renderscript.Type;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import underearnersanonymous.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import java.util.ArrayList;
import java.util.Arrays;

public class PieChartFragment extends DialogFragment {
    public static final String CATEGORIES = "categories";

    private GraphicalView mChartView;
    /** Colors to be used for the pie slices. */
    private static int[] COLORS = new int[] { Color.GRAY, Color.BLUE, Color.MAGENTA, Color.CYAN, Color.RED, Color.GREEN};
    /** The main series that will include all the data. */
    private CategorySeries mSeries = new CategorySeries("");
    /** The main renderer for the main dataset. */
    private DefaultRenderer mRenderer = new DefaultRenderer();
    private ArrayList<String> mCategories = new ArrayList<>(Arrays.asList("Time Drunkeness", "Business", "Vision", "Recreation", "Service", "Self Care"));

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.pie_chart_fragment, container, false);
        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.container);
        getDialog().setTitle(R.string.title_report);
        ArrayList<Integer> entries = getArguments().getIntegerArrayList(CATEGORIES);

        // set the start angle for the first slice in the pie chart
        mRenderer.setStartAngle(180);
        // display values on the pie slices
        mRenderer.setDisplayValues(true);
        for(int i = 0; i < entries.size(); i++) {
            int squares = entries.get(i);
            mSeries.add(mCategories.get(i)+" ("+getTime(squares)+")", squares);
            SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
            renderer.setColor(COLORS[(mSeries.getItemCount() - 1) % COLORS.length]);
            mRenderer.addSeriesRenderer(renderer);
        }
        mChartView = ChartFactory.getPieChartView(getActivity(), mSeries, mRenderer);
        linearLayout.addView(mChartView);
//        mChartView.repaint();
        return rootView;
    }

    private String getTime(int squares) {
        int mins = squares * 15;
        int hrs = mins/60;
        int remMins = mins - hrs*60;
        String strHrs = "", strMins = "";
        if(hrs==1){
            strHrs = hrs+"hr ";
        } else {
            strHrs = hrs+"hrs ";
        }

        if (mins == 1) {
            strMins = remMins+"min";
        } else {
            strMins = remMins+"mins";
        }

        return strHrs+strMins;
    }
}
