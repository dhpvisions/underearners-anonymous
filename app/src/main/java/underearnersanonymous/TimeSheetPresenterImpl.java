package underearnersanonymous;

import android.content.Context;

import java.util.ArrayList;

public class TimeSheetPresenterImpl implements TimeSheetPresenter {
    private final TimeSheetInteractor mInteractor;
    private TimeSheetActivity mView;

    public TimeSheetPresenterImpl(Context context, TimeSheetActivity timeSheetActivity) {
        mView = timeSheetActivity;
        mInteractor = new TimeSheetInteractorImpl(context, this);
    }

    @Override
    public ArrayList<CompositeTextView> loadData(int date) {
        return mInteractor.loadData(date);
    }

    @Override
    public void insertTimeEntry(int date, int position, String description, String category) {
        mInteractor.insertTimeEntry(date, position, description, category);
    }

}
