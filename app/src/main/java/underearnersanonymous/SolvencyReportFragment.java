package underearnersanonymous;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SolvencyReportFragment extends DialogFragment{
    public static final String EXPENSES = "expenses";
    private static final String TAG = SolvencyReportFragment.class.getSimpleName();
    private TableLayout mTable;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.solvency_report, container, false);
        getDialog().setTitle(getString(R.string.spending_record));
        mTable = (TableLayout) rootView.findViewById(R.id.table_layout);
        initTable();
        ArrayList<Expense> monthlyExpenses = getArguments().getParcelableArrayList(EXPENSES);
        if(monthlyExpenses != null){
            createExpenseCategories(monthlyExpenses);
        }
        return rootView;
    }

    private void createExpenseCategories(ArrayList<Expense> monthlyExpenses) {
        List<String> categories = Arrays.asList(getResources().getStringArray(R.array.categories));

        float[][] categoryCnt = new float[categories.size()][4];
        for (Expense expense : monthlyExpenses){
            int idx = categories.indexOf(expense.category);
            if(idx != -1){
                int week = getWeek(expense.getDate());
                categoryCnt[idx][week]+= Float.parseFloat(expense.getAmount());
            }
        }

        for(int i = 1; i < categories.size(); i++){
            insertRow(categories.get(i), categoryCnt[i]);
        }
    }

    private void insertRow(String category, float[] weekCnt) {
        TableRow rowHeader = new TableRow(getActivity());
        CompositeLinearLayout view = initTextView(category, R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(Float.toString(weekCnt[0]), R.drawable.cell_white);
        rowHeader.addView(view);
        view = initTextView(Float.toString(weekCnt[1]), R.drawable.cell_white);
        rowHeader.addView(view);
        view = initTextView(Float.toString(weekCnt[2]), R.drawable.cell_white);
        rowHeader.addView(view);
        view = initTextView(Float.toString(weekCnt[3]), R.drawable.cell_white);
        rowHeader.addView(view);
        view = initTextView(Float.toString(weekCnt[0] + weekCnt[1] + weekCnt[2] +weekCnt[3]),  R.drawable.cell_white);
        rowHeader.addView(view);
        mTable.addView(rowHeader);

    }

    //date in the format dd/MM, week 1 = 1st-7th, week 2 = 8th-14th, week 3 = 15th -21st, week 4 = 22nd onwards
    private int getWeek(String date) {
        int day = Integer.parseInt(date.split("/")[0]);
        if (1 <= day && day <= 7) {
            return 0;
        } else if (8 <= day && day <= 14) {
            return 1;
        } else if (15 <= day && day <= 21) {
            return 2;
        } else {
            return 3;
        }
    }

    private void initTable() {
        mTable.removeAllViews();
        initHeader();
    }

    private void initHeader() {
        TableRow rowHeader = new TableRow(getActivity());
        CompositeLinearLayout view = initTextView(getString(R.string.week), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(getString(R.string.one), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(getString(R.string.two), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(getString(R.string.three), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(getString(R.string.four), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        view = initTextView(getString(R.string.total),  R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        mTable.addView(rowHeader);
    }

    private CompositeLinearLayout initTextView(String text, int background) {
        return initTextView(text, 75, background, Gravity.CENTER);
    }

    private CompositeLinearLayout initTextView(String text, int width, int background, int gravity) {
        CompositeLinearLayout linearLayout = new CompositeLinearLayout(getActivity());
        linearLayout.setText(text);
        linearLayout.setWidth(width);
        linearLayout.setSingleLine();
        linearLayout.setPadding(2, 2, 2, 2);
        if(background != 0) {
            linearLayout.setBackgroundResource(background);
        }
        linearLayout.setMinimumHeight(33);
        linearLayout.setGravity(gravity);
        linearLayout.setMinTextSize(14);
        linearLayout.addEllipse(false);
        return linearLayout;
    }

}
