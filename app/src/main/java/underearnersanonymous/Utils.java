package underearnersanonymous;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Utils {

    public static void saveStateToPreferences(Context context, String string) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor1 = settings.edit();
        editor1.putString(Constants.MODE, string);
        editor1.apply();
    }
}
