package underearnersanonymous;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class CompositeTextView extends AutoSizeTextView{
    String category, description;

    public CompositeTextView(Context context) {
        super(context);
    }

    public CompositeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CompositeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
