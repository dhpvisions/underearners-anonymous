package underearnersanonymous;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.LinearLayout;

import java.util.Objects;

public class CompositeLinearLayout extends LinearLayout {

    public CompositeLinearLayout(Context context) {
        super(context);
        this.addView(new CompositeTextView(context));
    }


    public void setText(String text) {
        getTextView().setText(text);
        getTextView().resizeText();
    }

    private CompositeTextView getTextView() {
        return (CompositeTextView) this.getChildAt(0);
    }

    public String getDescription() {
        return getTextView().getDescription();
    }

    public void setGravity(int gravity){
        getTextView().setGravity(gravity);
        super.setGravity(gravity);
    }

    public void setWidth(int width) {
        getTextView().setWidth(width);
        super.setMinimumWidth(width);
    }

    public void setMinimumHeight(int height){
        getTextView().setMinimumHeight(height);
        getTextView().setHeight(height);
        super.setMinimumHeight(height);
    }


    public void setSingleLine() {
        getTextView().setSingleLine();
    }

    public void setPadding(int left, int top, int right, int bottom){
        getTextView().setPadding(left, top, right, bottom);
    }

    public void setTypeface(Typeface typeface, int style) {
        getTextView().setTypeface(typeface, style);
    }

    public void setTextColor(int color) {
        getTextView().setTextColor(color);
    }

    public void setDescription(String description) {
        getTextView().setDescription(description);
    }


    public void setCategory(String category) {
        getTextView().setCategory(category);
    }

    public void setText(CharSequence text) {
        getTextView().setText(text);
    }

    public CharSequence getText() {
        return getTextView().getText();
    }

    public String getCategory() {
        return getTextView().getCategory();
    }

    public void setMinTextSize(int i) {
        getTextView().setMinTextSize(i);
    }

    public void setEllipse(String s) {
        getTextView().setEllipsis(s);
    }

    public void addEllipse(boolean b) {
        getTextView().setAddEllipsis(b);
    }
}
