package underearnersanonymous;

public class Constants {
    public static final int CELLS = 80; // 20 rows x 4 columns
    public static final String DBDATE = "yyyyMMdd";
    public static final String MODE = "mode";
    public static final String SOLVENCY = "solvency";
    public static final String TIMERECORDING = "timerecording";
}
