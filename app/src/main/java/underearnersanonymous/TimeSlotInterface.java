package underearnersanonymous;

import java.util.ArrayList;

public interface TimeSlotInterface {
    public void applyEntry(String description, ArrayList<Integer> slots, String category);
}
