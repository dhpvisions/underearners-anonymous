package underearnersanonymous;

import java.util.ArrayList;

public interface SolvencyPresenter {
    long addExpense(String date, String description, String category, String amount);

    ArrayList<Expense> loadExpenses();

    void deleteExpense(long _id);

    ArrayList<Expense> getExpensesForMonth();
}
