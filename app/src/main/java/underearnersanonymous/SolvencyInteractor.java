package underearnersanonymous;

import java.util.ArrayList;

public interface SolvencyInteractor {
    long addExpense(String date, String description, String category, String amount);

    ArrayList<Expense> loadExpenses();

    void deleteExpense(long id);

    ArrayList<Expense> getExpensesForMonth();
}
