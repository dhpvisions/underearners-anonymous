package underearnersanonymous;

import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SolvencyActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CELL_PADDING = 2;
    private static final String TAG = SolvencyActivity.class.getSimpleName();
    private static final int WIDTH_DESC = 180;
    private Date mCurrentDate;
    private TableLayout mTable;
    private SolvencyPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solvency);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.solvency);

        mTable = (TableLayout) findViewById(R.id.table_layout);
        mPresenter = new SolvencyPresenterImpl(this);
        ImageView btnTime = (ImageView) findViewById(R.id.btn_time);

        btnTime.setClickable(true);

        btnTime.setOnClickListener(this);

        mCurrentDate = new Date();
        initTable();

        ImageView fab = (ImageView) findViewById(R.id.fab);
        fab.setClickable(true);
        fab.setOnClickListener(this);
        loadDataFromDB();
    }

    private void loadDataFromDB() {
        ArrayList<Expense> expenses = mPresenter.loadExpenses();
        for(Expense expense: expenses){
            addTableRow(expense);
        }
    }


    private void initTable() {
        mTable.removeAllViews();
        initHeader();

//        addTableRow("12 noon", R.drawable.cell_white);
    }

    private void initHeader() {
        addHeaderRow();
    }

    private void addHeaderRow() {
        TableRow rowHeader = new TableRow(this);
        CompositeLinearLayout view = initTextView(getString(R.string.date), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        TableRow.LayoutParams params = (TableRow.LayoutParams) view.getLayoutParams();
        params.weight = 1;
        view.setLayoutParams(params);
        view = initTextView(getString(R.string.description), WIDTH_DESC,  R.drawable.cell_white, Gravity.CENTER);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        params = (TableRow.LayoutParams) view.getLayoutParams();
        params.weight = 3;
        view.setLayoutParams(params);
        view = initTextView(getString(R.string.category), R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        params = (TableRow.LayoutParams) view.getLayoutParams();
        params.weight = 1;
        view.setLayoutParams(params);
        view = initTextView(getString(R.string.amount),  R.drawable.cell_white);
        view.setTextColor(Color.BLACK);
        view.setTypeface(null, Typeface.BOLD);
        rowHeader.addView(view);
        params = (TableRow.LayoutParams) view.getLayoutParams();
        params.weight = 1;
        view.setLayoutParams(params);
        mTable.addView(rowHeader);
    }


    private void addTableRow(Expense expense) {
        addTableRow(expense.getId(), expense.getDate(), expense.getDescription(), expense.getCategory(), expense.getAmount());
    }

    private void addTableRow(long id, String date, String description, String category, String amount) {
        final CompositeTableRow rowHeader = new CompositeTableRow(this);
        rowHeader.set_id(id);
        rowHeader.addView(initTextView(date, R.drawable.cell_white));
        CompositeLinearLayout tempTv = initTextView(description, WIDTH_DESC, R.drawable.cell_white, Gravity.CENTER);
        rowHeader.addView(tempTv);
        tempTv = initTextView(category.length() > 9 ? category.substring(0,9) : category, R.drawable.cell_white);
        rowHeader.addView(tempTv);
        Float fAmount = Float.parseFloat(amount);
        tempTv = initTextView(String.format(Locale.getDefault(), "%.2f", fAmount), 95, R.drawable.cell_white, Gravity.RIGHT);
        rowHeader.addView(tempTv);
        rowHeader.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Safety check can only delete last row
                if(mTable.getChildAt(mTable.getChildCount()-1) != rowHeader){
                    return true;
                }

                ViewParent parent = rowHeader.getParent();
                if (parent!=null && parent instanceof ViewGroup){
                    ((ViewGroup) parent).removeView(rowHeader);
                }
                mPresenter.deleteExpense(rowHeader.get_id());
                return true;
            }
        });
        mTable.addView(rowHeader);
    }

    private CompositeLinearLayout initTextView(String text, int background) {
        return initTextView(text, 95, background, Gravity.CENTER);
    }

    private CompositeLinearLayout initTextView(String text, int width, int background, int gravity) {
        CompositeLinearLayout linearLayout = new CompositeLinearLayout(this);
        linearLayout.setText(text);
        linearLayout.setWidth(width);
        linearLayout.setSingleLine();
        linearLayout.setPadding(CELL_PADDING, CELL_PADDING, CELL_PADDING, CELL_PADDING);
        if(background != 0) {
            linearLayout.setBackgroundResource(background);
        }
        linearLayout.setMinimumHeight(33);
        linearLayout.setGravity(gravity);
        return linearLayout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab:
                AddExpense fragment = new AddExpense();
                FragmentManager fm =  getFragmentManager();
                fragment.show(fm, "Add Expense");
                break;
            case R.id.btn_time:
                onBackPressed();
                break;
            case R.id.btn_money:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_solvency, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        case R.id.action_report:
            SolvencyReportFragment fragment = new SolvencyReportFragment();
            ArrayList<Expense> expensesForMonth = mPresenter.getExpensesForMonth();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(SolvencyReportFragment.EXPENSES, expensesForMonth);
            fragment.setArguments(bundle);
            FragmentManager fm = getFragmentManager();
            fragment.show(fm, "Solvency Monthly Report");
            return true;
        }

        return true;
    }

            @Override
    public void onBackPressed() {
        Utils.saveStateToPreferences(this, Constants.TIMERECORDING);
        super.onBackPressed();
    }

    public void addExpense(String description, String category, String amount) {
        String date = new SimpleDateFormat("dd/MM", Locale.getDefault()).format(new Date());
        long id = mPresenter.addExpense(date, description, category, amount);
        addTableRow(id, date, description, category, amount);
    }
}
