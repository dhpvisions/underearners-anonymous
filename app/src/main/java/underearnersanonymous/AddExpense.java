package underearnersanonymous;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddExpense extends DialogFragment implements View.OnClickListener {
    private Spinner spnCategories;
    private EditText etDescription, etAmount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_money_entry, container, false);
        spnCategories = (Spinner) rootView.findViewById(R.id.categories);
        ArrayAdapter<String> categories = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.categories));
        spnCategories.setAdapter(categories);
        getDialog().setTitle(R.string.add_expense);

        Button btnOK = (Button) rootView.findViewById(R.id.confirm);
        btnOK.setOnClickListener(this);

        etDescription = (EditText) rootView.findViewById(R.id.description);
        etAmount = (EditText) rootView.findViewById(R.id.amount);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        if(validData()) {
            ((SolvencyActivity) getActivity()).addExpense(etDescription.getText().toString(),
                                                          spnCategories.getSelectedItem().toString(),
                                                            etAmount.getText().toString());
            dismiss();
        }
    }

    private boolean validData() {
        boolean isValid = true;
        if(etDescription.getText().toString().equals("")){
            etDescription.setError(getString(R.string.enter_description));
            isValid = false;
        }

        if(spnCategories.getSelectedItemPosition() == 0){
            Toast.makeText(getActivity(), getString(R.string.select_category), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if(etAmount.getText().toString().equals("")){
            etAmount.setError(getString(R.string.enter_amount));
            isValid = false;
        }

        return isValid;
    }
}
