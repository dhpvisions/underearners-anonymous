package underearnersanonymous;

import java.util.ArrayList;

public interface TimeSheetPresenter {
    ArrayList<CompositeTextView> loadData(int date);

    void insertTimeEntry(int date, int position, String description, String category);
}
