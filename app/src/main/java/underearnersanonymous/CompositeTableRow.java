package underearnersanonymous;

import android.content.Context;
import android.widget.TableRow;

public class CompositeTableRow extends TableRow {
    long _id;

    public CompositeTableRow(Context context) {
        super(context);
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }
}
