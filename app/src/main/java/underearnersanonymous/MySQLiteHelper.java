package underearnersanonymous;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_TIMESHEET = "timesheet";
    public static final String TABLE_CATEGORY = "category";
    private static final String TABLE_EXPENSES = "expenses";
    public static final String COLUMN_ID = "_id";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_DESC = "description";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_AMOUNT = "amount";

    private static final String DATABASE_NAME = "timesheet.db";
    private static final int DATABASE_VERSION = 3;

    // Database creation sql statement
    private static final String TIMESHEET_CREATE = getTimesheetCreateStatement();
    private static final String CATEGORY_CREATE = getCategoryCreateStatement();
    private static final String TAG = MySQLiteHelper.class.getSimpleName();
    private static MySQLiteHelper mInstance = null;
    private final Context mContext;

    private static String getCategoryCreateStatement() {
        String statement = "create table "
                + TABLE_CATEGORY + "( " + COLUMN_ID
                + " integer primary key, ";

        for (int i = 0; i < Constants.CELLS -1; i++){
            statement += "_" + i + " text, ";
        }
        statement += "_" + (Constants.CELLS - 1) + " text);";
        return statement;
    }

    private static String getTimesheetCreateStatement() {
        String statement = "create table "
                + TABLE_TIMESHEET + "( " + COLUMN_ID
                + " integer primary key, ";

        for (int i = 0; i < Constants.CELLS -1; i++){
            statement += "_" + i + " text, ";
        }
        statement += "_" + (Constants.CELLS - 1) + " text);";
        return statement;
    }

    private MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        Log.d(TAG, getTimesheetCreateStatement());
    }

    public static MySQLiteHelper getInstance(Context context) {
        if(mInstance == null){
            mInstance = new MySQLiteHelper(context);
        }

        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(TIMESHEET_CREATE);
        database.execSQL(CATEGORY_CREATE);
        database.execSQL("create table "
                + TABLE_EXPENSES + "( " + COLUMN_ID
                + " integer primary key autoincrement, "
                + COLUMN_DATE + " text not null, "
                + COLUMN_DESC + " text not null, "
                + COLUMN_CATEGORY + " text not null, "
                + COLUMN_AMOUNT + " text not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        db.execSQL("create table "
                + TABLE_EXPENSES + "( " + COLUMN_ID
                + " integer primary key autoincrement, "
                + COLUMN_DATE + " text not null, "
                + COLUMN_DESC + " text not null, "
                + COLUMN_CATEGORY + " text not null, "
                + COLUMN_AMOUNT + " text not null);");
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMESHEET);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
//        onCreate(db);
    }

    public ArrayList<CompositeTextView> loadData(int date) {
        SQLiteDatabase mydatabase = this.getReadableDatabase();
        Cursor cursor = mydatabase.rawQuery("Select * from " + TABLE_TIMESHEET + " where " + COLUMN_ID +
                                                " = " + date, null);
        ArrayList<CompositeTextView> data = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                for(int i = 1; i < Constants.CELLS + 1; i++){  //i = 0 corresponds to _ID
                    CompositeTextView ctv = new CompositeTextView(mContext);
                    ctv.setText(cursor.getString(i));
                    data.add(ctv);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        cursor = mydatabase.rawQuery("Select * from " + TABLE_CATEGORY + " where " + COLUMN_ID +
                " = " + date, null);
        if (cursor.moveToFirst()) {
            do {
                for(int i = 1; i < Constants.CELLS + 1; i++){  //i = 0 corresponds to _ID
                    data.get(i - 1).setCategory(cursor.getString(i));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return data;
    }

    public void insertTimeEntry(int date, int position, String description) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues insertValues = new ContentValues();
        insertValues.put(COLUMN_ID, Integer.toString(date));
        insertValues.put("_" + position, description);
        int id = (int) db.insertWithOnConflict(TABLE_TIMESHEET, null, insertValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update(TABLE_TIMESHEET, insertValues, "_id=?", new String[] {Integer.toString(date)});
        }
    }

    public void insertCategory(int date, int position, String category) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues insertValues = new ContentValues();
        insertValues.put(COLUMN_ID, Integer.toString(date));
        insertValues.put("_" + position, category);
        int id = (int) db.insertWithOnConflict(TABLE_CATEGORY, null, insertValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            db.update(TABLE_CATEGORY, insertValues, "_id=?", new String[] {Integer.toString(date)});
        }
    }

    public long addExpense(String date, String description, String category, String amount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues insertValues = new ContentValues();
        insertValues.put(COLUMN_DATE, date);
        insertValues.put(COLUMN_DESC, description);
        insertValues.put(COLUMN_CATEGORY, category);
        insertValues.put(COLUMN_AMOUNT, amount);
        return db.insertWithOnConflict(TABLE_EXPENSES, null, insertValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public ArrayList<Expense> loadExpenses() {
        SQLiteDatabase mydatabase = this.getReadableDatabase();
        Cursor cursor = mydatabase.rawQuery("Select * from " + TABLE_EXPENSES, null);
        ArrayList<Expense> data = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Expense expense = new Expense();
                expense.setId(cursor.getInt(0));
                expense.setDate(cursor.getString(1));
                expense.setDescription(cursor.getString(2));
                expense.setCategory(cursor.getString(3));
                expense.setAmount(cursor.getString(4));
                data.add(expense);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }

    public void deleteExpense(long id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_EXPENSES, COLUMN_ID+"=?", new String[]{Long.toString(id)});
    }

    public ArrayList<Expense> getExpensesForMonth() {
        String month = new SimpleDateFormat("MM", Locale.getDefault()).format(new Date());
        SQLiteDatabase mydatabase = this.getReadableDatabase();
        Cursor cursor = mydatabase.rawQuery("Select * from " + TABLE_EXPENSES + " where " + COLUMN_DATE + " like '%/" + month+"'", null);
        ArrayList<Expense> data = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Expense expense = new Expense();
                expense.setId(cursor.getInt(0));
                expense.setDate(cursor.getString(1));
                expense.setDescription(cursor.getString(2));
                expense.setCategory(cursor.getString(3));
                expense.setAmount(cursor.getString(4));
                data.add(expense);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }
}
